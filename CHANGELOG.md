# Changelog

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.0] - 2018-05-20
### Added
- !discord command

## [0.4.2] - 2018-05-09
### Changed
- Changed the !waifu command to add a -chan suffix to names for increased weebness

## [0.4.1] - 2018-04-13
### Changed
- Fix crash caused by invalid quote numbers

## [0.4.0] - 2018-04-13
### Added
- !quote command

### Changed
- Replaced the waifu module with a more generic user module

## [0.3.0] - 2018-03-31
### Added
- Twitch config
- !version command
- !help command

### Changed
- !waifu command more reliable (I hope)

## [0.2.0] - 2018-03-25
### Added
- !waifu command

## [0.1.0] - 2018-03-22
### Added
- Basic bot functionality
