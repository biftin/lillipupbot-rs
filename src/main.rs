extern crate irc;
extern crate itertools;
extern crate rand;
extern crate reqwest;
extern crate serde;
extern crate serde_json;
extern crate toml;

#[macro_use]
extern crate serde_derive;

mod quote;
mod user;

use irc::client::prelude::*;
use irc::proto::CapSubCommand;
use std::collections::VecDeque;
use std::fs::File;
use std::io::prelude::*;
use std::sync::{Arc, Mutex};

use quote::Quotes;
use user::UserList;

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

#[derive(Deserialize)]
struct TwitchConfig {
    channel: String,
}

fn main() {
    println!("Starting LillipupBot version: {}...", VERSION);

    let twitch_config = load_twitch_config();

    let irc_config = Config::load("irc_config.toml").unwrap();

    let mut reactor = IrcReactor::new().unwrap();
    let client = reactor.prepare_client_and_connect(&irc_config).unwrap();
    client.identify().unwrap();

    let irc_channel = irc_config.channels.unwrap()[0].clone();

    client
        .send(Command::CAP(
            None,
            CapSubCommand::REQ,
            None,
            Some("twitch.tv/membership".to_owned()),
        ))
        .unwrap();

    let hellomessage = format!("Hello, LillipupBot version {} here.", VERSION);
    client
        .send_privmsg(irc_channel.as_str(), hellomessage.as_ref())
        .unwrap();

    let user = Arc::new(Mutex::new(UserList::new(twitch_config.channel.clone())));
    let quotes = Arc::new(Mutex::new(Quotes::new(String::from("quotes.json"))));

    reactor.register_client_with_handler(client, move |client, message| {
        print!("IRC {}", message);

        match &message.command {
            &Command::PRIVMSG(_, ref msg) => {
                let nick = message.source_nickname().unwrap();
                match msg.as_ref() {
                    "!hello" => {
                        let outmsg = format!("Hi, {}!", nick);
                        client
                            .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                            .unwrap();
                    }
                    "!bye" => {
                        let outmsg = format!("Bye, {}!", nick);
                        client
                            .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                            .unwrap();
                    }
                    "!gw2" => {
                        let outmsg =
                            "Lilly is playing on the EU servers. \
                             You can add her, if you'd like to join her: ♥ kiwiciel.4672 ♥";
                        client
                            .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                            .unwrap();
                    }
                    "!lol" => {
                        let outmsg = "You can add Lilly in League of Legends: \
                                      ♥ Calliopé ♥ She is playing on the EUW servers.";
                        client
                            .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                            .unwrap();
                    }
                    "!discord" => {
                        let outmsg = "You can join Lilly's cute Discord server here: \
                                      https://discord.gg/VESAuDB";
                        client
                            .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                            .unwrap();
                    }
                    "!donations" => {
                        let outmsg =
                            "If you don't want to subscribe, \
                             but you still want to support Lilly's stream, \
                             you can donate using this link: https://streamlabs.com/oklilly ♥";
                        client
                            .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                            .unwrap();
                    }
                    "!killyourself" => {
                        let outmsg = format!(
                            "No, {}, I won't. I'm a happy bot. \
                             Here, have some love: <3 <3 <3",
                            nick
                        );
                        client
                            .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                            .unwrap();
                    }
                    "!waifu" => {
                        let waifuchan = user.lock().unwrap().get_random_user();

                        let outmsg = format!("{}, your new waifu is: {}-chan", nick, waifuchan);
                        client
                            .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                            .unwrap();
                    }
                    "!version" => {
                        let outmsg =
                            format!("I'm currently running lillypupbot-rs version: {}", VERSION);
                        client
                            .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                            .unwrap();
                    }
                    "!help" => {
                        let outmsg = "For a list of commands, see here: \
                                      https://gitlab.com/biftin/lillipupbot-rs/wikis/Home";
                        client
                            .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                            .unwrap();
                    }
                    _ => {
                        let mut command_split: VecDeque<&str> = msg.split(" ").collect();
                        let command = command_split.pop_front().unwrap();

                        match command {
                            "!quote" => {
                                quotes.lock().unwrap().handle_command(
                                    command_split,
                                    client,
                                    nick,
                                    irc_channel.clone(),
                                    user.lock().unwrap(),
                                );
                            }
                            _ => (),
                        }
                    }
                }
            }
            _ => (),
        }

        Ok(())
    });

    reactor.run().unwrap();
}

fn load_twitch_config() -> TwitchConfig {
    let mut content = String::new();
    let mut f = File::open("twitch_config.toml").expect("Twitch config file not found");
    f.read_to_string(&mut content).unwrap();

    let config: TwitchConfig = toml::from_str(content.as_str()).unwrap();

    config
}
