use itertools;
use serde_json;
use user;

use irc::client::prelude::*;
use std::collections::VecDeque;
use std::fs::File;
use std::path::Path;
use std::sync::MutexGuard;

#[derive(Serialize, Deserialize)]
struct QuotesConfig {
    quotes: Vec<String>,
}

pub struct Quotes {
    quotes_config: QuotesConfig,
    file_name: String,
}

impl Quotes {
    pub fn new(file_name: String) -> Self {
        let mut instance = Quotes {
            quotes_config: QuotesConfig { quotes: Vec::new() },
            file_name,
        };

        instance.load_quotes();

        instance
    }

    fn load_quotes(&mut self) {
        match File::open(Path::new(&self.file_name)) {
            Ok(file) => {
                let config: QuotesConfig =
                    serde_json::from_reader(file).unwrap_or(QuotesConfig { quotes: Vec::new() });
                self.quotes_config = config;
            }
            _ => self.quotes_config = QuotesConfig { quotes: Vec::new() },
        }
    }

    pub fn handle_command(
        &mut self,
        mut command_vec: VecDeque<&str>,
        client: &IrcClient,
        nick: &str,
        irc_channel: String,
        mut users: MutexGuard<user::UserList>,
    ) {
        match command_vec.pop_front() {
            Some(subcmd) => match subcmd {
                "add" => match command_vec.front() {
                    Some(_) => match users.get_user_type(String::from(nick)) {
                        user::UserType::Moderator(_) => {
                            let joined = itertools::join(command_vec, " ");
                            self.add_quote(joined);

                            let outmsg = format!("Quote added ({})", nick);
                            client
                                .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                                .unwrap();
                        }
                        _ => {
                            let outmsg = format!("You are not allowed to add Quotes. (Mods only)");
                            client
                                .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                                .unwrap();
                        }
                    },
                    None => {
                        let outmsg = format!("Missing quote ({})", nick);
                        client
                            .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                            .unwrap();
                    }
                },
                "get" => match command_vec.pop_front() {
                    Some(num) => match String::from(num).parse::<usize>() {
                        Ok(num_i) => match self.get_quote(num_i) {
                            Some(quote) => {
                                client
                                    .send_privmsg(irc_channel.as_str(), quote.as_ref())
                                    .unwrap();
                            }
                            None => {
                                let outmsg = format!("Quote not found ({})", nick);
                                client
                                    .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                                    .unwrap();
                            }
                        },
                        _ => {
                            let outmsg = format!("Invalid quote number ({})", nick);
                            client
                                .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                                .unwrap();
                        }
                    },
                    None => {
                        let outmsg = format!("Missing quote number ({})", nick);
                        client
                            .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                            .unwrap();
                    }
                },
                "reload" => {
                    let outmsg = format!("Reloading quotes database... ({})", nick);
                    client
                        .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                        .unwrap();
                    self.load_quotes();
                }
                _ => {
                    let outmsg = format!("Unknown subcommand '{}' ({})", subcmd, nick);
                    client
                        .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                        .unwrap();
                }
            },
            None => {
                let outmsg = format!("Missing subcommand ({})", nick);
                client
                    .send_privmsg(irc_channel.as_str(), outmsg.as_ref())
                    .unwrap();
            }
        }
    }

    pub fn get_quote(&self, quote_num: usize) -> Option<String> {
        if quote_num >= self.quotes_config.quotes.len() {
            return None;
        }

        Some(self.quotes_config.quotes[quote_num].clone())
    }

    pub fn add_quote(&mut self, quote: String) {
        self.quotes_config.quotes.push(quote);

        match File::create(Path::new(&self.file_name)) {
            Ok(file) => match serde_json::to_writer_pretty(file, &self.quotes_config) {
                Ok(_) => (),
                Err(err) => {
                    println!("QUOTE : error trying to write to quotes file: {:?}", err);
                }
            },
            Err(err) => {
                println!("QUOTE : error trying to open quotes file: {:?}", err);
            }
        }
    }
}
