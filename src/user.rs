use rand;
use serde_json;

use rand::Rng;
use reqwest::Client;
use std::time::{SystemTime, UNIX_EPOCH};

pub struct UserList {
    chatters: Chatters,
    last_update: SystemTime,
    channel: String,
    client: Client,
}

#[derive(Deserialize)]
struct TMIChattersResponse {
    chatters: Chatters,
}

impl Default for TMIChattersResponse {
    fn default() -> Self {
        TMIChattersResponse {
            chatters: Chatters {
                moderators: Vec::new(),
                staff: Vec::new(),
                admins: Vec::new(),
                global_mods: Vec::new(),
                viewers: Vec::new(),
            },
        }
    }
}

#[derive(Deserialize)]
struct Chatters {
    moderators: Vec<String>,
    staff: Vec<String>,
    admins: Vec<String>,
    global_mods: Vec<String>,
    viewers: Vec<String>,
}

pub enum UserType {
    Moderator(String),
    Staff(String),
    Admin(String),
    GlobalMod(String),
    Viewer(String),
    NonExistent(),
}

impl UserList {
    pub fn new(channel: String) -> UserList {
        let chatters = Chatters {
            moderators: Vec::new(),
            staff: Vec::new(),
            admins: Vec::new(),
            global_mods: Vec::new(),
            viewers: Vec::new(),
        };

        let mut userlist = UserList {
            chatters: chatters,
            last_update: UNIX_EPOCH,
            channel: channel,
            client: Client::new(),
        };

        userlist.update_list();

        userlist
    }

    pub fn get_random_user(&mut self) -> String {
        self.update_list();

        let mut rng = rand::thread_rng();

        let mut full_vec: Vec<String> = Vec::new();
        full_vec.append(&mut self.chatters.moderators.clone());
        full_vec.append(&mut self.chatters.staff.clone());
        full_vec.append(&mut self.chatters.admins.clone());
        full_vec.append(&mut self.chatters.global_mods.clone());
        full_vec.append(&mut self.chatters.viewers.clone());

        rng.choose(&full_vec)
            .unwrap_or(&String::from("no users"))
            .clone()
    }

    pub fn get_user_type(&mut self, user: String) -> UserType {
        self.update_list();

        let mod_iter = self.chatters.moderators.iter();
        for val in mod_iter {
            if val == user.as_str() {
                return UserType::Moderator(user);
            }
        }

        let staff_iter = self.chatters.staff.iter();
        for val in staff_iter {
            if val == user.as_str() {
                return UserType::Staff(user);
            }
        }

        let admin_iter = self.chatters.admins.iter();
        for val in admin_iter {
            if val == user.as_str() {
                return UserType::Admin(user);
            }
        }

        let global_iter = self.chatters.global_mods.iter();
        for val in global_iter {
            if val == user.as_str() {
                return UserType::GlobalMod(user);
            }
        }

        let viewer_iter = self.chatters.viewers.iter();
        for val in viewer_iter {
            if val == user.as_str() {
                return UserType::Viewer(user);
            }
        }

        UserType::NonExistent()
    }

    fn update_list(&mut self) {
        let now = SystemTime::now();
        let duration = now.duration_since(self.last_update).unwrap().as_secs();

        if duration > 60 {
            let url = format!("https://tmi.twitch.tv/group/user/{}/chatters", self.channel);

            match self.client.get(url.as_str()).send() {
                Ok(mut response) => {
                    let data: TMIChattersResponse =
                        serde_json::from_str(response.text().unwrap().as_ref()).unwrap_or_default();
                    self.chatters = data.chatters;
                    self.last_update = SystemTime::now();
                }
                _ => {
                    println!("Unable to retrieve user list.");
                }
            }
        }
    }
}
